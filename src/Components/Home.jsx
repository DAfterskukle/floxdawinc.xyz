import React, { useState, useEffect } from "react";

const Home = () => {
  const [tick, setTick] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {
      fetch("https://data.exchange.coinjar.com/products/BTCAUD/ticker")
        .then((res) => res.json())
        .then((data) => {
          setTick(data);
        })
        .catch(console.log);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  return (
    <div>
      <h2>{tick.last}</h2>
      <h2>{tick.current_time}</h2>
    </div>
  );
};

export default Home;
